export class Produto {
	constructor(
		public uuid:string,
		public name:string,
		public icon:string,
		public quantidade:number,
		public unidade:string,
		public bought:boolean){}
	
}
