import { Component, OnInit } from '@angular/core';
import { NavController, ToastController} from '@ionic/angular';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ProdutoProviderService } from '../../services/produto-provider.service';
import { Produto } from '../../model/produto';
@Component({
  selector: 'app-add-produto',
  templateUrl: './add-produto.page.html',
  styleUrls: ['./add-produto.page.scss'],
})
export class AddProdutoPage implements OnInit {

  constructor(private produtosProvider:ProdutoProviderService, private navCtrl: NavController, private toastController: ToastController) { }

  ngOnInit() {
  }

  saveProduto(produtoForm:any){
  	if(produtoForm.invalid){
  		console.log("Error on form")
  		this.presentToast("Error on form!");
  	}else{
  		let newProduto = new Produto(this.produtosProvider.genProdutoUUID(produtoForm.value.name, produtoForm.value.quantidade),
  			produtoForm.value.name, 
  			produtoForm.value.icon,
  			produtoForm.value.quantidade,
        produtoForm.value.unidade,
  			false);
  	

  	this.produtosProvider.save(newProduto);
  	this.produtosProvider.notifyChange();

  	this.presentToast("Produto '" + newProduto.name + "' adicionado!");
  	this.navCtrl.back();
  }

}

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      position: 'top',
      duration: 3000,
      color: 'secondary'
    });
    toast.present();
  }

}
