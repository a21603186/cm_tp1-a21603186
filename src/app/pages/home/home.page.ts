import { Component } from '@angular/core';
import { Events } from '@ionic/angular';
import { ProdutoProviderService } from '../../services/produto-provider.service';
import { Produto } from '../../model/produto';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage{

  public produtos: Array<Produto>;

  constructor(private produtosProvider: ProdutoProviderService, private events: Events) { 
 
   }

  ngOnInit() {
  	this.produtos = this.produtosProvider.getProdutos();
  }

  toggleBought(produto:Produto){

    this.produtosProvider.buy(produto);

  	this.produtosProvider.notifyChange();
    return produto.bought;

  }

  toggleRemove(produto:Produto){

    this.produtosProvider.remove(produto);
    this.produtosProvider.notifyChange();
    
  }

  onSearchInputChanged(event:any){

  	let searchQuery = event.target.value;
  	if(searchQuery && searchQuery.trim() != ''){

  		this.produtos = this.produtosProvider.getProdutos().filter((produto) => {
  			return (produto.name.toLowerCase().indexOf(searchQuery.toLowerCase()) > -1);
  		})

  	}else{
  		this.produtos = this.produtosProvider.getProdutos();
  	}
  }

}
