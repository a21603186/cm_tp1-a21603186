import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Events } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { Produto } from '../model/produto';
@Injectable({
  providedIn: 'root'
})
export class ProdutoProviderService {

  private produtos: Array<Produto>;

  constructor(private http: Http, private events : Events) {

  	this.produtos = [];

   }

   public getProduto(produtoUUID: string){
   	let produtoFound = this.getProdutos().find((produto) => {return produto.uuid == produtoUUID;});

   	if(produtoFound){
   		console.log("PRODUTO ENCONTRADO");
   	}else{
   		console.log("PRODUTO NÃO ENCONTRADO");
   	}
   	return produtoFound;
   }

   public getProdutos(){
   	if(this.produtos.length == 0){
   		this.loadData();
   	}return this.produtos;
   }

   public save(produto:Produto){
   	this.produtos.push(produto);
   	console.log("Produto '" + produto.name + "' saved in memory");
   }



   loadData(){
   	this.http.get('assets/data/produtos.json').pipe(map(res => res.json())).subscribe(data => {
   		for(let i=0; i < data.length; ++i){
   			this.produtos.push(
   				new Produto(data[i].uuid, data[i].name, data[i].icon, data[i].quantidade, data[i].unidade, data[i].bought));

   		}

   	
   	this.notifyChange();
   }, err => {
   		console.log("ERROR LOADING DATA");

   }
   );

   }

  

   public remove(produto:Produto){
         const index: number = this.produtos.indexOf(produto);
       if (index !== -1) {
           this.produtos.splice(index, 1);
       } 

   }

   public buy(produto:Produto){

     if(produto.bought){
       produto.bought=false;
       console.log("Produto nao comprado");
     }
     if(!produto.bought){
       produto.bought=true;
       console.log("Produto comprado");
     }

   }

   public notifyChange(){
   	this.events.publish('produto-data-changed');
   	console.log("Publishing event 'produto-data-changed'");
   }

   public genProdutoUUID(produtoName: string, produtoQuantidade: number) {
   let uuid = this.uuid(produtoName.length, produtoQuantidade);
   console.log("UUID generated: " + uuid);
   return uuid;
 }
 //Código externo:https://gist.github.com/LeverOne/1308368 que gera um random uuid
 uuid(a,b){for(b=a='';a++<36;b+=a*51&52?(a^15?8^Math.random()*(a^20?16:4):4).toString(16):'-');return b}
}
