import { TestBed } from '@angular/core/testing';

import { ProdutoProviderService } from './produto-provider.service';

describe('ProdutoProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProdutoProviderService = TestBed.get(ProdutoProviderService);
    expect(service).toBeTruthy();
  });
});
